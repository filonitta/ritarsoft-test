var gulp = require('gulp'),
	gutil = require('gulp-util'),
	rename = require('gulp-rename'),
	coffee = require('gulp-coffee'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	copy = require('gulp-copy');

gulp.task('sass', function () {
	gulp.src('src/sass/style.scss')
		.pipe(sass({ outputStyle: 'compressed' }))
		.pipe(rename('main.css'))
		.pipe(gulp.dest('public/css'));
});

gulp.task('coffee', function() {
	gulp.src('src/coffee/*.coffee')
		.pipe(coffee({bare: true}).on('error', gutil.log))
		.pipe(gulp.dest('src/js/'));

	gulp.start('scripts');
});

gulp.task('library', function() {
	gulp.src([
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/angular/angular.min.js',
		'bower_components/angular-material/angular-material.min.js'
	])
	.pipe(concat('lib.js'))
	//.pipe(uglify())
	.pipe(gulp.dest('public/js/'));
});

gulp.task('scripts', function() {
	gulp.src([
		'src/js/*.js'
	])
	//.pipe(concat('main.js'))
	//.pipe(uglify())
	.pipe(gulp.dest('public/js/'));
});

gulp.task('htmlmin', function() {
	gulp.src('src/*.html')
		.pipe(htmlmin({
			removeComments: true,
			collapseWhitespace: true,
			minifyJS: true,
			minifyCSS: true
		}))
		.pipe(gulp.dest('public'));
});

gulp.task('copyfonts', function() {
	 gulp.src('bower_components/font-awesome/fonts/**')
	 .pipe(gulp.dest('public/css/fonts'));
});
gulp.task('copy', function() {
	 gulp.src('bower_components/angular-material/angular-material.css')
	 .pipe(gulp.dest('public/css'));
});

gulp.task('build', function() {
	gulp.start('copy');
	gulp.start('sass');
	gulp.start('library');
	gulp.start('scripts');
	gulp.start('htmlmin');

	gulp.start('default');
});

gulp.task('default', function(){
	//gulp.watch('src/coffee/*.coffee', ['coffee']);
	gulp.watch('src/js/*.js', ['scripts']);
	gulp.watch('src/sass/*.scss', ['sass']);
	gulp.watch('src/*.html', ['htmlmin']);
});