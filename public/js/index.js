var app = angular.module('ritarSoft', []);

app.directive('draggable', function() {
	return function(scope, element) {
		var el = element[0];

		el.draggable = true;

		el.addEventListener('dragstart', function(e) {
			e.dataTransfer.effectAllowed = 'move';
			e.dataTransfer.setData('Element', this.id);
			this.classList.add('drag');
			return false;
		}, false);

		el.addEventListener('dragend', function(e) {
			this.classList.remove('drag');
			return false;
		}, false);
	}
});

app.directive('droppable', function() {
	return {
		scope: {
			drop: '&' // parent
		},
		link: function(scope, element) {
			var el = element[0];

			el.addEventListener('dragover', function(e) {
				e.dataTransfer.dropEffect = 'move';
				if (e.preventDefault) e.preventDefault();
				this.classList.add('over');
				return false;
			}, false);

			el.addEventListener('dragenter', function(e) {
				this.classList.add('over');
				return false;
			}, false);

			el.addEventListener('dragleave', function(e) {
				this.classList.remove('over');
				return false;
			}, false);

			el.addEventListener('drop', function(e) {
				this.classList.remove('over');

				var binId = this.id,
					item = document.getElementById(e.dataTransfer.getData('Element')),
					parentId = item.parentNode.id;

				// this.appendChild(item);

				scope.$apply(function(scope) {
					var fn = scope.drop();
					if ('undefined' !== typeof fn) {
						fn(item.id, parentId, binId);
					}
				});

			}, false);
		}
	}
});

app.controller('todoCtrl', function($scope) {
	var getList = function(alias) {
		var list;
		switch (alias) {
			case 'todo-region': list = $scope.todoList; break;
			case 'progress-region': list = $scope.progressList; break;
			case 'done-region': list = $scope.doneList; break;
		}
		return list;
	}

	$scope.todoList = [
		{
			id: 'task' + Math.floor((Math.random() * 100)),
			title:'First',
			description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas quisquam eveniet id velit! Impedit est, quis saepe, rerum nostrum architecto illo deleniti optio ex obcaecati aliquam, fugit porro explicabo soluta?'
		}
	];

	$scope.progressList = [];
	$scope.doneList = [];


	$scope.addTask = function() {
		if (!this.title) return;

		$scope.todoList.push({
			id: 'task' + Math.floor((Math.random() * 100)),
			title: this.title,
			description: this.description
		});
		this.title = this.description = '';
	}

	$scope.removeTask = function(region) {
		var targetData = getList(region);
		for (var i = 0; i <= targetData.length; i++) {
			if (targetData[i]['id'] == this.task.id) {
				targetData.splice(i, 1);
				break;
			}
		}
	};

	$scope.editTask = function($event, data) {
		var el = $event.target,
			task = this.task;

		$event.target.setAttribute('contenteditable', true);
		$event.target.style.cursor = 'text';
		$event.target.focus();

		var range;
		if (document.body.createTextRange) {
			range = document.body.createTextRange();
			range.moveToElementText($event.target);
			range.select();
		}
		else if (window.getSelection) {
			var selection = window.getSelection();
			range = document.createRange();
			range.selectNodeContents($event.target);
			selection.removeAllRanges();
			selection.addRange(range);
		}

		$event.target.addEventListener('keypress', function(event) {
			if (event.keyCode == 13 && data == 'title') this.blur();
		});

		$event.target.addEventListener('blur', function(event) {
			task[data] = this.innerText;
			this.removeAttribute('contenteditable');
			$event.target.style.cursor = '';
		}, false);
	};

	$scope.handleDrop = function(taskId, parentId, targetId) {
		var item,
			parentData = getList(parentId);
			targetData = getList(targetId);

		for (var i = 0; i <= parentData.length; i++) {
			item = parentData[i];
			if (item.id == taskId) {
				targetData.push( (parentData.splice(i, 1)[0]) );
				break;
			}
		};
	}
})